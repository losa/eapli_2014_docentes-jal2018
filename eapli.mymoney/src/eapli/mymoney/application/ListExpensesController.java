/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.Book;
import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.MonthPage;
import eapli.mymoney.persistence.BookRepository;
import eapli.mymoney.persistence.MonthPageRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fernando
 */
public class ListExpensesController extends BaseController {

	public List<Expense> getExpensesOfMonth(int year, int month) {
		MonthPageRepository repo = Persistence.getRepositoryFactory().
			getMonthPageRepository();

		MonthPage page = repo.findForDate(year, month);
		if (null == page) {
			return new ArrayList();
		} else {
			return page.expenses();
		}
	}

	private List<MonthPage> getAllMonthPages() {
		final BookRepository repo = Persistence.getRepositoryFactory().
			getBookRepository();
		final Book theBook = repo.theBook();
		final List<MonthPage> monthPages = theBook.monthPages();
		return monthPages;
	}

	public List<Expense> getAllExpenses() {
		List<MonthPage> months = getAllMonthPages();
		List<Expense> expenses = new ArrayList<>();
		for (MonthPage month : months) {
			expenses.addAll(month.expenses());
		}
		return expenses;
	}
}
