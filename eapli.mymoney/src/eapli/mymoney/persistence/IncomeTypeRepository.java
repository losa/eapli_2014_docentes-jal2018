/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.domain.IncomeType;
import java.util.List;

/**
 *
 * @author Nuno
 */
public interface IncomeTypeRepository {

	/**
	 * adds a new income type to the repository
	 *
	 * @param incomeType
	 * @throws IllegalsStateException if there is already an income type with
	 * the same description
	 */
	void add(IncomeType incomeType);

	long size();

	List<IncomeType> all();
}
