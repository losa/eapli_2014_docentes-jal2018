/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

//import eapli.framework.patterns.DomainEntity;
import eapli.framework.patterns.DomainEntity;
import eapli.util.Validations;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Entity
public class ExpenseType implements DomainEntity<String> {

	//TODO há bastante duplicação entre IncomeType e ExpenseType
	// deviamos fazer um refactoring
	@Id
	private String text;

	public ExpenseType() {
	}

	public ExpenseType(final String text) {
		if (Validations.isNullOrWhiteSpace(text)) {
			throw new IllegalArgumentException();
		}
		this.text = text;
	}

	/**
	 * returns the description text of this expense type
	 *
	 * this method is only provided for user output
	 *
	 * @return
	 */
	public String description() {
		return text;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other == this) {
			return true;
		}
		if (!(other instanceof ExpenseType)) {
			return false;
		}
		ExpenseType etOther = (ExpenseType) other;
		return this.text.equals(etOther.text);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 41 * hash + (this.text != null ? this.text.hashCode() : 0);
		return hash;
	}

	@Override
	public String id() {
		return text;
	}
}
