/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.events;

import eapli.mymoney.domain.Income;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class IncomeRegistered extends MovementRegistered {

	// TODO check if it makes sense to use domain objects or if we should use raw data in events
	private final Income theIncome;

	public IncomeRegistered(Income anIncome) {
		super(anIncome.when());
		this.theIncome = anIncome;
	}
}
