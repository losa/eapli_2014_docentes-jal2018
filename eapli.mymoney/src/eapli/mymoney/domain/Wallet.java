/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.patterns.DomainEntity;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
@Entity
public class Wallet implements DomainEntity<Long> {

	@Id
	@GeneratedValue
	private Long id;

	@OneToMany(cascade = CascadeType.ALL)
	private List<PaymentMean> paymentMeans = new ArrayList<>();

	public Wallet() {
		ensureCashExists();
	}

	/**
	 *
	 * @param bankName
	 * @param accountNumber
	 */
	public void registerBankCheckBook(final String bankName,
									  final long accountNumber) {
		final PaymentMean checkBook = new BankCheckBook(bankName, accountNumber);
		paymentMeans.add(checkBook);
	}

	public void registerCreditCard(final String bankName,
								   final Calendar validity,
								   final long accountNumber,
								   final String issuerName) {
		final PaymentMean creditCard = new CreditCard(bankName, validity, accountNumber, issuerName);
		paymentMeans.add(creditCard);
	}

	public void registerDebitCard(final String bankName,
								  final Calendar validity,
								  final long accountNumber) {
		final PaymentMean debitCard = new DebitCard(bankName, validity, accountNumber);
		paymentMeans.add(debitCard);
	}

	private void ensureCashExists() {
		for (PaymentMean mean : paymentMeans) {
			if (mean instanceof Cash) {
				return;
			}
		}
		final PaymentMean cash = new Cash();
		paymentMeans.add(cash);
	}

	public List<PaymentMean> paymentMeans() {
		return Collections.unmodifiableList(paymentMeans);
	}

	@Override
	public Long id() {
		return id;
	}
}
