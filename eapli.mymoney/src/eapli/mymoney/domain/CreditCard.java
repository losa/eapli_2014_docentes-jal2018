/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.util.Validations;
import java.util.Calendar;
import javax.persistence.Entity;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
@Entity
public class CreditCard extends PaymentCard {

	private String issuerEntity;

	public CreditCard() {
	}

	public CreditCard(final String bankName,
					  final Calendar validity,
					  final long cardNumber,
					  final String issuerEntity) {
		super(bankName, validity, cardNumber);

		if (Validations.isNullOrEmpty(issuerEntity) || Validations.
			isNullOrWhiteSpace(issuerEntity)) {
			throw new IllegalArgumentException();
		}

		this.issuerEntity = issuerEntity;
	}

	@Override
	public String description() {
		return "CREDIT CARD -" + super.description();
	}

}
