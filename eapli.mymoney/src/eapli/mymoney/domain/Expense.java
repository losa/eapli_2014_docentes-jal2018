/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.model.Money;
import eapli.framework.patterns.DomainEntity;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Entity
public class Expense implements DomainEntity<Long> {

	//TODO refactor to base class when we start working with Incomes
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String what;

	@ManyToOne(cascade = CascadeType.MERGE)
	private Money howMuch;

	@Temporal(javax.persistence.TemporalType.DATE)
	private Calendar when;

	@ManyToOne(cascade = CascadeType.REFRESH)
	private ExpenseType type;

	@OneToOne(cascade = CascadeType.ALL)
	private Payment payment;

	//TODO será que o construtor já devia receber o objeto Payment? criado pelo MonthPage
	public Expense(final String what, final Money howMuch, final Calendar when,
				   final ExpenseType type, final PaymentMean paymentMean,
				   final String info) {
		if (howMuch == null || what == null || when == null || type == null
			|| paymentMean == null || what.trim().isEmpty()
			|| howMuch.lessThanOrEqual(Money.euros(0))) {
			throw new IllegalArgumentException();
		}
		this.howMuch = howMuch;
		this.what = what;
		this.when = when;
		this.type = type;
		this.payment = createPayment(paymentMean, info);
	}

	/**
	 * provided for ORM tool only
	 */
	public Expense() {
	}

	public Money amount() {
		return howMuch;
	}

	public Calendar when() {
		return when;
	}

	public Payment createPayment(PaymentMean mean, String chequeNumber) {
		if (mean instanceof BankCheckBook) {
			return new CheckPayment(mean, chequeNumber);
		}
		return new Payment(mean);
	}

	// TODO não deviamos usar toString() para imprimir objetos; apenas devia servir para debug
	// no nosso caso como é uma aplicação de consola trata-se na realidade de lógica de apresentação
	// for output in UI classes
	@Override
	public String toString() {
		return "\nExpense: "
			+ "\n\tDescription: " + what
			+ "\n\tAmount: " + howMuch.amount()
			+ "\n\tDate: " + when.get(Calendar.DAY_OF_MONTH) + "/"
			+ (when.get(Calendar.MONTH) + 1) + "/"
			+ when.get(Calendar.YEAR)
			+ "\n\tExpense Type: " + type.description()
			+ "\n\tPayment: " + payment.description();
	}

	@Override
	public Long id() {
		return id;
	}
}
